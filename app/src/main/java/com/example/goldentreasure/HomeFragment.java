package com.example.goldentreasure;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class HomeFragment extends Fragment {

    private TextView btn_day,btn_week,btn_month,btn_year;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.activity_home_fragment,container,false);

        btn_day=view.findViewById(R.id.btnDay);
        btn_week=view.findViewById(R.id.btnWeek);
        btn_month=view.findViewById(R.id.btnMonth);
        btn_year=view.findViewById(R.id.btnYear);

        btn_day.setOnClickListener(new click());
        btn_week.setOnClickListener(new click());
        btn_month.setOnClickListener(new click());
        btn_year.setOnClickListener(new click());

        return view;

    }
    public class click implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnDay:
                    btn_day.setBackgroundResource(R.drawable.fill_background);
                    btn_week.setBackgroundResource(R.drawable.blank_background);
                    btn_month.setBackgroundResource(R.drawable.blank_background);
                    btn_year.setBackgroundResource(R.drawable.blank_background);
                    break;
                case R.id.btnWeek:
                    btn_week.setBackgroundResource(R.drawable.fill_background);
                    btn_day.setBackgroundResource(R.drawable.blank_background);
                    btn_month.setBackgroundResource(R.drawable.blank_background);
                    btn_year.setBackgroundResource(R.drawable.blank_background);
                    break;
                case R.id.btnMonth:
                    btn_month.setBackgroundResource(R.drawable.fill_background);
                    btn_day.setBackgroundResource(R.drawable.blank_background);
                    btn_week.setBackgroundResource(R.drawable.blank_background);
                    btn_year.setBackgroundResource(R.drawable.blank_background);
                    break;
                case R.id.btnYear:
                    btn_year.setBackgroundResource(R.drawable.fill_background);
                    btn_day.setBackgroundResource(R.drawable.blank_background);
                    btn_week.setBackgroundResource(R.drawable.blank_background);
                    btn_month.setBackgroundResource(R.drawable.blank_background);
                    break;
                default:

            }
        }
    }
}