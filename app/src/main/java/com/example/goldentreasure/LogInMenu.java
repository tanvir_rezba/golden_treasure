package com.example.goldentreasure;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.goldentreasure.flash.flash_screen;

public class LogInMenu extends AppCompatActivity {

    private TextView button_login,button_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_menu);

        button_login=findViewById(R.id.tv_btn_login_menu);
        button_signup=findViewById(R.id.tv_btn_signup_menu);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LogInMenu.this, login.class); startActivity(i);
            }
        });

        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LogInMenu.this, register.class); startActivity(i);
            }
        });

    }
}