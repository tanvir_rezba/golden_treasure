package com.example.goldentreasure;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.FragmentManager;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottom_nav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Start with Home Fragment.....................
        getSupportFragmentManager().beginTransaction().replace(R.id.FL_container, new HomeFragment()).commit();
        //Initial Bottom Navigation..............................
        bottom_nav = findViewById(R.id.bottom_nav);
        bottom_nav.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottom_nav.setSelectedItemId(R.id.home);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment setFragment = null;

            FragmentManager fragmentManager;

            switch (item.getItemId()) {
                case R.id.home:
                    setFragment = new HomeFragment();
                    break;
                case R.id.deposit:
                    setFragment = new DepositFragment();
                    break;
                case R.id.transaction:
                    setFragment = new TransactionFragment();
                    break;
                case R.id.menu_settings:
                    setFragment = new SettingFragment();
                    break;
                case R.id.support:
                    setFragment = new SupportFragment();
                    break;
            }
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.FL_container, setFragment).commit();
            return true;
        }
    };
}