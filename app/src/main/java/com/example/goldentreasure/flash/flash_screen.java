package com.example.goldentreasure.flash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.goldentreasure.LogInMenu;
import com.example.goldentreasure.R;
import com.example.goldentreasure.login;

public class flash_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_screen);


        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                Intent i = new Intent(flash_screen.this, LogInMenu.class); startActivity(i);
                finish();}
            }, 2000);
    }
}